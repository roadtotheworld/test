﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using RoadToTheWorldDomain.Entities;
namespace RoadToTheWorldData
{
    public class RoadToTheWorldContext : DbContext
    {

        public RoadToTheWorldContext()
            : base("RoadToTheWorldDB")
         {
             Database.SetInitializer<RoadToTheWorldContext>(new RoadToTheWorldContextInitializer()); 
         }
        public DbSet<User> Users { get; set; }
        public DbSet<Event> Events { get; set; }

        public DbSet<Message> Messages { get; set; }
        public DbSet<Comment> Comments { get; set; }
        public DbSet<Picture> Pictures { get; set; }
        public virtual void Commit()
        {
            base.SaveChanges();
        }


    }
}
