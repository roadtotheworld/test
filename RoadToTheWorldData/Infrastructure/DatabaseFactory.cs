﻿using RoadToTheWorldData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyFinanceData.Infrastructure
{
    public class DatabaseFactory : Disposable, IDatabaseFactory 
    {
        private RoadToTheWorldContext dataContext = null;
        public RoadToTheWorldContext Get() 
        {
            return dataContext ?? (dataContext = new RoadToTheWorldContext(); 
        } 
        protected override void DisposeCore()
        {
            if (dataContext != null)          
                dataContext.Dispose();
        } 
    }
}
