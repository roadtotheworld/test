﻿
using RoadToTheWorldData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyFinanceData.Infrastructure
{
    public class UnitOfWork : IUnitOfWork
    {
        private RoadToTheWorldContext dataContext; DatabaseFactory dbFactory;
        public UnitOfWork(DatabaseFactory dbFactory)
        {
            this.dbFactory = dbFactory;
        }
        private IExpenseRepository expenseRepository; 
        IExpenseRepository IUnitOfWork.ExpenseRepository 
        { 
            get 
            { 
                return expenseRepository ?? (expenseRepository = new ExpenseRepository(dbFactory));
            }
        }      
        private ICategoryRepository categoryRepository; 
        ICategoryReposi IUnitOfWork.CategoryRepository 
        { 
            get 
            { 
                return categoryRepository ?? (categoryRepository = new CategoryRepository(dbFactory));
            }
        }      
        private IUserReposito userRepository; 
        IUserRepository IUnitOfWork.UserRepository 
        { 
            get 
            { 
                return userRepository ?? (userRepository = new UserRepository(dbFactory));
            }
        }      
        private IRoleRepository roleRepository; 
        IRoleRepository IUnitOfWork.RoleRepository 
        { 
            get 
            {
                return roleRepository ?? (roleRepository = new RoleRepository(dbFactory));
            }
        }      
        private IProductRepository productRepository; 
        IProductRepository IUnitOfWork.ProductRepository 
        {
            get 
            { 
                return productRepository ?? (productRepository = new ProductRepository(dbFactory));
            }
        }
        protected RoadToTheWorldContext DataContext 
        {
            get
            {
                return dataContext ?? (dataContext = dbFactory.Get());
            }
        }      
        public void Commit() 
        { 
            DataContext.Commit(); 
        }
    }  
 
}
